﻿using IPZ_Proj.Exceptions;
using IPZ_Proj.Service;
using NUnit.Framework;

namespace TestProject
{
    class UserTests
    {
        private IUserService userService;

        [SetUp]
        public void Setup()
        {
            userService = new UserService();
        }

        [Test]
        public void WhenGetUsers_ThenResponseNotNull()
        {
            Assert.AreEqual(false, userService.GetUsers() == null);
        }

        [Test]
        public void WhenChangeEmail_ThenInvalidEmailException()
        {
            Assert.Throws<InvalidEmailException>((() => userService.ChangeEmail("wrong_email")));
        }

        [Test]
        public void WhenChangeLogin_ThenInvalidLoginException()
        {
            Assert.Throws<InvalidLoginException>((() => userService.ChangeLogin("wrong_login")));
        }

        [Test]
        public void WhenChangePassword_ThenInvalidPasswordException()
        {
            Assert.Throws<InvalidPasswordException>((() => userService.ChangePassword("f")));
        }

        [Test]
        public void WhenChangeRole_ThenInvalidRoleException()
        {
            Assert.Throws<InvalidRoleException>((() => userService.ChangeRole("aaa")));
        }
    }
}
