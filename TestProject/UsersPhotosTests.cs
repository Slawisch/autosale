﻿using IPZ_Proj.Exceptions;
using IPZ_Proj.Service;
using NUnit.Framework;

namespace TestProject
{
    class UsersPhotosTests
    {
        private IUsersPhotosService usersPhotosService;

        [SetUp]
        public void Setup()
        {
            usersPhotosService = new UsersPhotosService();
        }

        [Test]
        public void WhenGetUsersPhotos_ThenResponseNotNull()
        {
            Assert.AreEqual(false, usersPhotosService.GetUsersPhotos() == null);
        }

        [Test]
        public void WhenChangePhoto_ThenInvalidPathException()
        {
            Assert.Throws<InvalidPathException>((() => usersPhotosService.ChangePhoto("/nfvbn.txt")));
        }
    }
}
