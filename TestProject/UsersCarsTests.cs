﻿using IPZ_Proj.Exceptions;
using IPZ_Proj.Service;
using NUnit.Framework;

namespace TestProject
{
    class UsersCarsTests
    {
        private IUsersCarService usersCarService;

        [SetUp]
        public void Setup()
        {
            usersCarService = new UsersCarService();
        }

        [Test]
        public void WhenGetUsersCars_ThenResponseNotNull()
        {
            Assert.AreEqual(false, usersCarService.GetUsersCars() == null);
        }

        [Test]
        public void WhenChangeBrand_ThenEmptyStringException()
        {
            Assert.Throws<EmptyStringException>((() => usersCarService.ChangeBrand("")));
        }

        [Test]
        public void WhenChangeCondition_ThenConditionException()
        {
            Assert.Throws<ConditionException>((() => usersCarService.ChangeCondition("neeeew")));
        }

        [Test]
        public void WhenChangeFuel_ThenFuelException()
        {
            Assert.Throws<FuelException>((() => usersCarService.ChangeFuel("Gg")));
        }

        [Test]
        public void WhenChangeYear_ThenInvalidYearException()
        {
            Assert.Throws<InvalidYearException>((() => usersCarService.ChangeYear(-143)));
        }

        [Test]
        public void WhenChangeHP_ThenZeroException()
        {
            Assert.Throws<ZeroException>((() => usersCarService.ChangeHP(0)));
        }
    }
}
