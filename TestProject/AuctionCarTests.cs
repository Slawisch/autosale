﻿using IPZ_Proj.Service;
using IPZ_Proj.Exceptions;
using NUnit.Framework;

namespace TestProject
{
    class AuctionCarTests
    {
        private IAuctionCarService auctionCarService;

        [SetUp]
        public void Setup()
        {
            auctionCarService = new AuctionCarService();
        }

        [Test]
        public void WhenGetCars_ThenResponseNotNull()
        {
            Assert.AreEqual(false, auctionCarService.GetCars() == null);
        }

        [Test]
        public void WhenChangeBrand_ThenEmptyStringException()
        {
            Assert.Throws<EmptyStringException>((() => auctionCarService.ChangeBrand("")));
        }

        [Test]
        public void WhenChangeCondition_ThenConditionException()
        {
            Assert.Throws<ConditionException>((() => auctionCarService.ChangeCondition("neeeew")));
        }

        [Test]
        public void WhenChangeFuel_ThenFuelException()
        {
            Assert.Throws<FuelException>((() => auctionCarService.ChangeFuel("Gg")));
        }

        [Test]
        public void WhenChangeYear_ThenInvalidYearException()
        {
            Assert.Throws<InvalidYearException>((() => auctionCarService.ChangeYear(-143)));
        }

        [Test]
        public void WhenChangeHP_ThenZeroException()
        {
            Assert.Throws<ZeroException>((() => auctionCarService.ChangeHP(0)));
        }

    }
}
